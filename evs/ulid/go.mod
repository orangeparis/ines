module github.com/oklog/ulid/v2

require (
	github.com/oklog/ulid v1.3.1 // indirect
	github.com/pborman/getopt v0.0.0-20170112200414-7148bc3a4c30
)

go 1.13
