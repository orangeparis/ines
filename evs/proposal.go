package evs

import (
	"bitbucket.org/orangeparis/ines/evs/ulid"
	"time"
)

/*

	proposal

	an eventIterator to read events form a store

	an event writer to write event(s) to a store

		type Event struct {
			ID   string // an ulid id  eg 0000XSNQC8D56SJRAVX4EDS9NG or an err code begins with !
			Data []byte // opaque data , usually a json representation of an event
			Meta byte   // a meta that badger can handle to retrieve some metada
		}


*/

// read event from a source
type EventIterator interface {
	Next() (event *Event, err error)
}

// write events to sink
type EventWriter interface {
	Write(events ...Event) (err error)
}

//
//  implementations
//

//
//  WRITER
//

type BadgerEventWriter struct {
	*Store
	ttl time.Duration
}

func NewBadgerEventWriter(store *Store, ttl time.Duration) (w *BadgerEventWriter, err error) {
	return &BadgerEventWriter{store, ttl}, err
}

// implements EventWriter
func (w *BadgerEventWriter) Write(events ...Event) (err error) {

	ttl := w.TTL

	// open a database transaction
	txn := w.NewTransaction(true)

	for _, e := range events {

		// if e.Timestamp == nil {
		// 	e.Timestamp = time.Now()
		// }

		// create ulid with entropy
		ts, _ := e.GetTimestamp()
		data := e.Data
		meta := e.Meta

		id, err := ulid.New(ulid.Timestamp(ts), w.entropy)
		if err != nil {
			return err
		}
		// serialize the ULID to its binary form
		binID, err := id.MarshalBinary()
		if err != nil {
			return (err)
		}

		// add the insert operation to the transaction
		// and open a new transaction if this one is full
		// e := badger.NewEntry(binID, data)
		e := badger.NewEntry(binID, data).WithMeta(meta).WithTTL(ttl)

		if err := txn.SetEntry(e); err == badger.ErrTxnTooBig {

			if err := txn.Commit(); err != nil {
				return err
			}
			txn = w.NewTransaction(true)
			if err := txn.SetEntry(e); err != nil {
				return err
			}
		}
	}
	// flush the transaction
	if err := txn.Commit(); err != nil {
		return err
	}

	return err

}

//
//  ITERATOR
//
type BadgerEventIterator struct {
	*Store
	Start     time.Time
	Duration  time.Duration
	eventChan chan Event
}

func NewBadgerEventIterator(store *Store) (w *BadgerEventIterator, err error) {
	return &BadgerEventIterator{
		Store:     store,
		eventChan: make(chan Event),
	}, err
}

//func ( i * BadgerEventIterator) Next() ( event *Event, err error) {
//
//	var dontWait = true
//
//
//	// compute boundaries
//	b0, b1, err := UlidTimeBoundaries(start, duration)
//	if err != nil {
//		return nil, err
//	}
//	log.Printf("WindowKeys: boundary keys : [%s -> %s]\n", b0, b1)
//
//	go func() {
//
//		// set timeout with a margin
//		timer := time.NewTimer(duration + 5*time.Second)
//		defer timer.Stop()
//
//		starter := b0
//
//		// create badger view to collect events
//		err = s.View(func(txn *badger.Txn) error {
//
//			// create a Badger iterator with the default settings
//			opts := badger.DefaultIteratorOptions
//			opts.PrefetchSize = 10
//			it := txn.NewIterator(opts)
//			defer it.Close()
//
//			current := ""
//
//			// main loop
//			for {
//
//				// seek b0 mark
//				it.Seek(starter.Bytes())
//
//				// read loop
//				for ; it.Valid(); it.Next() {
//					item := it.Item()
//
//					// rebuild ulid from key bytes
//					k := item.Key()
//					x := ulid.ULID{}
//					x.UnmarshalBinary(k)
//					current = x.String()
//
//					//
//					// test end of time window
//					//
//					if string(current) > b1.String() {
//						// outside of window : push null event to indicate we have all data
//						e := NewErrorEvent("200", "OK")
//						log.Printf("last key: n=%s\n", current)
//						event <- e
//						return nil
//					}
//
//					// within the window: push to channel
//					e := Event{}
//					v := make([]byte, item.ValueSize())
//					value, err := item.ValueCopy(v)
//					if err != nil {
//						// cannot extract value return an error event
//						event <- NewErrorEvent("400", "cannot extract value")
//						return nil
//					}
//
//					// ok fill the event and send
//					e.Data = value
//					e.ID = current
//					event <- e
//				} // end of read loop
//
//				if dontWait == true {
//					return nil
//				}
//
//				// cross point
//				// watch for
//				//      ctx.Done() has parent asked for stop
//				//      it.next()  is there another record to read
//				//      timer
//
//				// check end condition
//				select {
//				case <-ctx.Done():
//					log.Println("Done.")
//					return nil
//				case <-timer.C:
//					log.Println("Timed out")
//					event <- NewErrorEvent("404", "timeout")
//					return nil
//				default:
//					// wait before launching a new read loop
//					time.Sleep(1 * time.Second)
//				}
//
//				// prepare a new read loop
//				log.Println("Next read loop")
//				if current != "" {
//					// set a new start
//					starter, err = ulid.Parse(current)
//					if err != nil {
//						event <- NewErrorEvent("400", "cannot convert curren to ulid")
//						return nil
//					}
//				}
//
//			} // end of main
//		})
//		if err != nil {
//			e := NewErrorEvent("500", "Failed to create view: "+err.Error())
//			event <- e
//			// close channel
//			// close(event)
//		}
//
//	}()
//
//
//	return
//}
