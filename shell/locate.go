package shell

import (
	"errors"
	"path/filepath"
)

var LocateBinPaths = []string{"./", "/snap/bin", "/usr/local/bin", "/usr/bin", "/bin", "/sbin"}

// look for an executable :  return absolute path or nil
func LocateBin(name string) (path string, err error) {

	for _, dir := range LocateBinPaths {
		pattern := filepath.Join(dir, name)
		matches, err := filepath.Glob(pattern)
		if err == nil {
			if len(matches) > 0 {
				path = matches[0]
				return path, err
			}
		}
	}
	// not Found
	err = errors.New("NotFound")
	return
}
