// +build linux
package shell

import (
	"bytes"
	"errors"
	//"io/ioutil"
	"log"
	"os"
	"os/exec"
	"os/user"
	"runtime"
	"strconv"
)

/*
	works on debian
*/

func Mkdirs(path string) error {
	return os.MkdirAll(path, 0777)
}

func NewAsUser(username string) (asUser *AsUser, err error) {

	// check linux
	if runtime.GOOS != "linux" {
		err = errors.New("Not a linux system")
		return
	}
	// check sudo ?
	u := new(AsUser)
	err = u.SetupUser(username)
	return
}

type AsUser struct {
	User *user.User
	// uid gid
	Uid int
	Gid int
}

func (u *AsUser) CheckSudo() bool {
	if os.Geteuid() != 0 {
		return false
	}
	return true
}

func (u *AsUser) MkdirAll(path string) error {

	err := os.MkdirAll(path, 0777)
	if err != nil {
		return err
	}
	err = os.Chown(path, u.Uid, u.Gid)
	return err
}

func (u *AsUser) SetupUser(username string) error {

	// command:  sudo adduser --disabled-password --gecos "" consul
	_, err := user.Lookup(username)
	if err != nil {
		// user not existent : create it
		cmd := exec.Command("adduser", "--disabled-password", "--gecos", "", username)
		var out bytes.Buffer
		cmd.Stdout = &out
		err := cmd.Run()
		if err != nil {
			log.Println(err.Error())
			return err
		}
		log.Printf("user %s created: %q\n", username, out.String())

		//err = u.AddToGroup("docker")
		//if err != nil {
		//	log.Println(err.Error())
		//	return err
		//}
		//log.Printf("user consul added to docker group: %q\n", out.String())

	}
	// load user
	user, err := user.Lookup(username)
	if err == nil {
		// user ok sett it
		u.User = user
		u.Uid, _ = strconv.Atoi(user.Uid)
		u.Gid, _ = strconv.Atoi(user.Gid)
	}
	return err
}

func (u *AsUser) CreateUser(username string) (user user.User, err error) {
	// debian system only

	// command:  sudo adduser --disabled-password --gecos "" consul

	// user not existent : create it
	cmd := exec.Command("adduser", "--disabled-password", "--gecos", "", username)
	var out bytes.Buffer
	cmd.Stdout = &out
	err = cmd.Run()
	if err != nil {
		log.Println(err.Error())
		return user, err
	}
	log.Printf("user %s created: %q\n", username, out.String())
	return user, err
}

// add user to a group
func (u *AsUser) AddToGroup(group string) error {

	log.Printf("add user %s to group %s\n", u.User.Username, group)
	// sudo gpasswd -a consul docker
	cmd := exec.Command("gpasswd", "-a", u.User.Username, group)
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		log.Println(err.Error())
		return err
	}
	return err
}

//func (u * AsUser) Exec( args ...string) error {
//
//	cmd := exec.Command(args)
//	var out bytes.Buffer
//	cmd.Stdout = &out
//	err := cmd.Run()
//	if err != nil {
//		log.Println(err.Error())
//		return err
//	}
//	return err
//}
