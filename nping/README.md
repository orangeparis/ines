# nping


a library for pinging nats services


nats method : get.<service>.ping



## client

    Ping( nc * nats.Conn , service string , timeout int) ( bool,error )
    
    or
    
     c, _ := NewClient(nats://127.0.0.1:4222)
     defer c.Close()
     ok , _ := c.Ping("service1", 1)
    
    
    
## service 


    c, _ := NewClient(nats://127.0.0.1:4222)
    defer c.Close()
    
    // handle ping method for service  call.<service>.ping
    sub, _ := c.HandlePingService("service1")
    defer sub.Unsubscribe()
    
    
    	