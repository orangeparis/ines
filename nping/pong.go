package nping

import (
	"fmt"
	nats "github.com/nats-io/nats.go"
	"github.com/pkg/errors"
)

/*

	handle ping service ( in replying PONG )


	usage:

		c,_ = NewClient( "nats://127.0.0.1:4222" )
		defer c.Close()

		sub , _ := c.HandlePingService( "service" )
		defer sub.Unsubscribe()


*/

// subscribes to get.<service>.ping
// and respond with PONG
func HandlePingService(nc *nats.Conn, service string) (sub *nats.Subscription, err error) {

	subject := PingSubject(service)

	// try to ping service , to check it is not already in use
	p, err := Ping(nc, service, 1)
	if p == true {
		err = errors.Errorf("Service [%s] already in use : ABORT", service)
		return sub, err
	}
	if err != nil {
		// an error which is not a timeout
		return sub, err
	}

	// ping service is free : handle it
	sub, err = nc.Subscribe(subject, func(m *nats.Msg) {
		_ = nc.Publish(m.Reply, []byte(pongBody))
	})

	// handle the access.<service>.ping for resgate compatibility
	//_, _ = HandlePingAccess(nc, service)

	return sub, err
}

// handle access.<service>
func HandlePingAccess(nc *nats.Conn, service string) (sub *nats.Subscription, err error) {

	// ping service is free : handle it  access.<service>
	subject := fmt.Sprintf("access.%s", service)
	access := `"result": {"get": true,"call": "set,ping"}`

	sub, err = nc.Subscribe(subject, func(m *nats.Msg) {
		_ = nc.Publish(m.Reply, []byte(access))
	})
	return
}
