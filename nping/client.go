package nping

import (
	"fmt"
	nats "github.com/nats-io/nats.go"
	"log"
)

var pingBody = `PING`
var pongBody = `"PONG"`

var natsTimeoutError = "nats: timeout"

// return subject get.<service>.ping
func PingSubject(service string) string {
	return fmt.Sprintf("call.%s.ping", service)
}

type Client struct {
	*nats.Conn
}

func (c *Client) Ping(service string, timeout int) (bool, error) {
	return Ping(c.Conn, service, timeout)
}

func (c *Client) HandlePingService(service string) (sub *nats.Subscription, err error) {
	return HandlePingService(c.Conn, service)
}

func NewClient(natsServer string) (client *Client, err error) {

	// open a nats connection
	nc, err := nats.Connect(natsServer)
	if err != nil {
		log.Printf("nping client: cannot open nats connection: %s", err.Error())
		return client, err
	}

	client = &Client{nc}
	return client, nil
}
