package systemd

import (
	"bytes"
	"io/ioutil"
	"log"
	"text/template"
)

/*

	configure systemd network to redirect dns for domain consul to consul dns (

	see: https://gist.github.com/kquinsland/5cdc63614a581d9b392f435740b58729

*/

var netdevFilename = "/etc/systemd/network/dummy0.netdev"
var netdevTemplate = `
# Creates a "dummy" network interface
# we'll configure this interface with a link-local address
# See: https://www.freedesktop.org/software/systemd/man/systemd.netdev.html
##
[NetDev]
Name=dummy0
Kind=dummy
`

type Arguments struct {
	ConsulAddr string
}

var networkFilename = "/etc/systemd/network/dummy0.network"
var networkTemplate = `
# We "find" the dummy0 interface we created and configure it with a link-local address
# See: https://www.freedesktop.org/software/systemd/man/systemd.network.html
##
[Match]
Name=dummy0
[Network]
Address={{.ConsulAddr}}/32
# We want these domains
Domains= ~consul.
# To go to the consul DNS server we'll bind to this address
DNS={{.ConsulAddr}}
`

func GetNetdevConfiguration() (filename string, content string) {
	filename = netdevFilename
	content = netdevTemplate
	return
}

func GetNetworkConfiguration(ip string) (filename string, content string) {
	filename = networkFilename

	s := &Arguments{
		ConsulAddr: ip,
	}
	// resolve template
	text := ""
	tmpl, err := template.New("network").Parse(netdevTemplate)
	if err != nil {
		log.Fatal("Failed to parse template ")
	}
	buf := bytes.NewBufferString(text)
	err = tmpl.Execute(buf, s)
	if err != nil {
		log.Fatal("Failed to execute template: " + err.Error())
	}
	return
}

func InstallNetworkConfiguration(consulIP string) (err error) {

	// create netdev : /etc/systemd/network/dummy0.netdev
	f, content := GetNetdevConfiguration()
	err = ioutil.WriteFile(f, []byte(content), 0644)
	if err != nil {
		return err
	}

	// create network :  "/etc/systemd/network/dummy0.network"
	f, content = GetNetworkConfiguration(consulIP)
	err = ioutil.WriteFile(f, []byte(content), 0644)
	if err != nil {
		return err
	}

	return
}
