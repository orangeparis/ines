package systemd

import (
	"bytes"
	"errors"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"text/template"
	"time"
)

/*

	configure <service> service for systemd   eg <service> ="nomad"

	files :
		/etc/systemd/system/<service>.d
		/etc/systemd/system/<service>.service

	commands :

		sudo systemctl enable <service>
		sudo systemctl start <service>

		sudo systemctl disable <Service>


	usage :
		service = SystemdService{}
		err := service.Install()
		err := service.Enable()


	TODO: add system ctl reload
		systemctl daemon-reload




*/

//const ConsulD = "/etc/systemd/system/consul.d"
//// /etc/systemd/system/consul.service
//const consulServiceFilename = "/etc/systemd/system/consul.service"
//const consulService

var ServiceTemplate = `# systemd service unit file
[Unit]
Description={{.Description}}
After=network-online.target
Wants=network-online.target

[Service]
Type=simple
User={{.User}}
Group={{.User}}
ExecStart={{.Executable}} {{.ExecArgs}}
ExecReload=/bin/kill -HUP $MAINPID
KillSignal=SIGINT
TimeoutStopSec=5
Restart=on-failure
SyslogIdentifier={{.Name}}

[Install]
WantedBy=multi-user.target
`

// sample ExecStart = "/usr/bin/consul agent -config-file=/etc/consul.d/config.json"

type SystemdService struct {
	Name        string // name of the service eg nomad
	Description string // eg Consul Service Discovery Agent

	User string // eg consul

	Executable string // eg /usr/bin/consul
	ExecArgs   string // eg  agent -config-file=/etc/consul.d/config.json"

	Directory string //  "/etc/systemd/system/<service>.d"
	Filename  string //  "/etc/systemd/system/consul.service"
	Unit      string // the systemd configuration file content <service>.service

	ServiceConfigurationFile string // /etc/consul.d/config.json

}

func NewSystemdService(name string, description string, user string, executable, execArgs string) (*SystemdService, error) {

	if user == "" {
		user = name
	}
	s := &SystemdService{
		Name:                     name,
		Description:              description,
		User:                     user,
		Executable:               executable,
		ExecArgs:                 execArgs,
		Directory:                "/etc/systemd/system/" + name + ".d",
		Filename:                 "/etc/systemd/system/" + name + ".service",
		ServiceConfigurationFile: "/etc/" + name + ".d/config.json",
	}
	// resolve template
	text := ""
	tmpl, err := template.New("server").Parse(ServiceTemplate)
	if err != nil {
		log.Fatal("Failed to parse template ")
	}
	buf := bytes.NewBufferString(text)
	err = tmpl.Execute(buf, s)
	if err != nil {
		log.Fatal("Failed to execute template: " + err.Error())
	}
	s.Unit = buf.String()
	// return buf.Bytes(), err

	return s, nil
}

// check presence of consul configuration file : /etc/<service>.d/config.json
func (s *SystemdService) IsConfigured() bool {

	// /etc/<service>.d/config.json
	filename := s.ServiceConfigurationFile
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return false
	}
	return true
}

// mkdir /etc/systemd/system/consul.d
// write consul.service /etc/systemd/system/consul.service
func (s *SystemdService) Install() error {

	if s.IsConfigured() != true {
		// cannot install service : service not configured
		err := errors.New("abort: service not configured")
		return err
	}
	// create /etc/systemd/system/consul.d if necessary
	if _, err := os.Stat(s.Directory); os.IsNotExist(err) {
		err := os.Mkdir(s.Directory, 0777)
		if err != nil {
			return err
		}
	}
	// create consul.service
	text := s.Unit
	err := ioutil.WriteFile(s.Filename, []byte(text), 0644)
	return err
}

// Setup : perform a reload / start /enable
func (s *SystemdService) Setup() error {
	// sudo systemctl daemon-reload
	// sudo systemctl enable <service>
	// sudo systemctl start <service>
	err := s.Reload()
	if err != nil {
		log.Printf("failed to reload service: %s", err.Error())
	}
	err = s.systemctl("restart")
	if err != nil {
		log.Printf("failed to start service: %s", err.Error())
		return err
	}
	err = s.systemctl("enable")
	if err != nil {
		log.Printf("failed to enable service: %s", err.Error())
	}
	return err
}

func (s *SystemdService) IsInstalled() bool {
	// check /etc/systemd/system/<service>.d exists
	if _, err := os.Stat(s.Directory); os.IsNotExist(err) {
		return false
	}
	// check consul.service /etc/systemd/system/<service>.service
	if _, err := os.Stat(s.Filename); os.IsNotExist(err) {
		return false
	}
	return true
}

// start and enable
func (s *SystemdService) Disable() error {

	// sudo systemctl stop consul
	// sudo systemctl disable consul
	err := s.systemctl("disable")
	if err != nil {
		return err
	}
	err = s.systemctl("stop")
	return err
}

// Reload : perform a systemctl daemon-reload
func (s *SystemdService) Reload() error {

	cmd := exec.Command("systemctl", "daemon-reload")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	log.Printf("systemctl daemon-reload : %q\n", out.String())
	if err != nil {
		log.Printf("failed to reload :%s", err.Error())
		return err
	}
	time.Sleep(1 * time.Second)
	return nil

}

func (s *SystemdService) systemctl(op string) error {
	// op is enable,start stop disable

	// check service is installed
	if s.IsInstalled() != true {
		err := errors.New("abort: service not installed")
		return err
	}
	cmd := exec.Command("systemctl", op, s.Name)
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	log.Printf("systemctl %s %s: %q\n", op, s.Name, out.String())
	if err != nil {
		log.Println(err.Error())
		return err
	}
	log.Printf("systemctl %s %s: %q\n", op, s.Name, out.String())
	return nil
}
