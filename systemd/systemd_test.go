package systemd_test

import (
	"bitbucket.org/orangeparis/ines/systemd"
	"testing"
)

func TestNomadSystemd(t *testing.T) {

	sv, err := systemd.NewSystemdService("nomad", "nomad cluster", "consul",
		"/usr/bin/nomad", "agent --config /etc/nomad.d/config.json")

	_ = err
	_ = sv

	return

}
