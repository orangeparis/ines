
# systemd a helper to install a binary as a systemd service



usage

    sv, err := systemd.NewSystemdService(
        "nomad",            // name
        "nomad cluster",    // description 
        "consul",           // user
    	"/usr/bin/nomad", "agent --config /etc/nomad.d/config.json"
    )
    
    // edit name of the service configuration file if necessary
    sv.ServiceConfigurationFile: "/etc/nomad.d/config.json",
    
    
    // install the files ( eg  "/etc/systemd/system/consul.service", /etc/systemd/system/consul.d) 
    err := service.Install()
    
    // start and enable service
    err := service.Setup()