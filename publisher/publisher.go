package publisher

// Publisher a generic data publisher ( []bytes )
type Publisher interface {
	Publish(topic string, messsage []byte) error
}
