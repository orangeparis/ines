# publishers 

## interface 

    type Publisher interface {
    	Publish(topic string, messsage []byte) error
    }
    
## usage 

    // create a nats publisher
    p, err := publisher.NewNatsPublisher("topic", nats://demo.nats.io:4222)
    if err != nil {
        log.Fatal(err.Error())
    }
    defer p.Close()
    
    p.Publish("sub-topic", []byte("my message"))
   

    
## publishers

* natspublisher publish messages to a nats server
* screenpublisher publish to screen ( for debug )
* nilpublisher fake publish ( for benchmark )
* filepublisher publish to file
* pipepublisher publish to linux pipe