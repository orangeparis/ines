package publisher

// NilPublisher : a nil publisher for benchmarking
type NilPublisher struct {
	Topic   string // root topic to indentify publisher
	Counter uint64 // nb of messages published
}

// NewNilPublisher : create a screen publisher with a root topic
func NewNilPublisher() *NilPublisher {
	return &NilPublisher{Topic: "nil"}

}

// Publish msg to screen
func (p *NilPublisher) Publish(topic string, message []byte) (err error) {
	p.Counter += 1
	return err
}
