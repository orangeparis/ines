package publisher

import (
	"fmt"
	"log"

	nats "github.com/nats-io/nats.go"
)

// NatsPublisher : a publisher to publish to screen ( for debug )
type NatsPublisher struct {
	*nats.Conn
	Topic   string // root topic to identify publisher
	Server  string // server or list of nats servers
	Counter uint64 // number of messages published
}

// NewNatsPublisher : create a screen publisher with a root topic
func NewNatsPublisher(topic string, server string) (*NatsPublisher, error) {
	if topic == "" {
		topic = "root."
	}
	p := &NatsPublisher{Topic: topic, Server: server}
	conn, err := nats.Connect(p.Server)
	if err != nil {
		log.Printf("cannot open nats connection: %s", err.Error())
		return p, err
	}
	p.Conn = conn
	msg := fmt.Sprintf("Nats publisher connected with : %s\n", p.Server)
	log.Println(msg)

	return p, err
}

// func (p NatsPublisher) Connect() (err error) {

// 	// connect to nats server
// 	p.Conn, err = nats.Connect(p.Server)
// 	if err != nil {
// 		log.Printf("cannot open nats connection: %s", err.Error())
// 		return err
// 	}
// 	msg := fmt.Sprintf("Nats publisher connected with : %s\n", p.Server)
// 	log.Println(msg)

// 	return err
// }

// Close : close nats connection
func (p *NatsPublisher) Close() {

	p.Conn.Close()
	p.Conn = nil
	log.Printf("Nats publisher closed\n")

}

// Publish msg to nats server
func (p *NatsPublisher) Publish(topic string, message []byte) (err error) {

	// compute full topic
	//topic = p.Topic + topic
	subject := fmt.Sprintf("%s.%s", p.Topic, topic)
	// publish to nats
	err = p.Conn.Publish(subject, message)
	if err == nil {
		// increment counter
		p.Counter += 1
	}
	//log.Printf("natspublisher: publish to [" + topic + "] the message:\n" + string(message) + "\n")
	return err

}

// nc, err = nats.Connect("tls://localhost:4443", nats.RootCAs("./configs/certs/ca.pem"))

// NewNatsTlsPublisher : create a nats tls publisher with a root topic
//  eg rootCa = "./configs/certs/ca.pem"
//  eg user =  "~/.nkeys/creds/op/admin/admin.creds"
func NewNatsTlsPublisher(topic string, server string, rootCA string, user string) (*NatsPublisher, error) {
	if topic == "" {
		topic = "root."
	}
	credentials := nats.UserCredentials(user)
	cert := nats.RootCAs(rootCA)
	p := &NatsPublisher{Topic: topic, Server: server}
	conn, err := nats.Connect(server, cert, credentials)
	//conn, err := nats.Connect(p.Server)
	if err != nil {
		log.Printf("cannot open nats connection: %s", err.Error())
		return p, err
	}
	p.Conn = conn
	msg := fmt.Sprintf("Nats publisher connected with : %s\n", p.Server)
	log.Println(msg)

	return p, err
}
