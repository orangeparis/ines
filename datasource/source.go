package datasource

/*

	define the DataSource interface

*/

type DataSource interface {
	Name() string          // source name
	Next() ([]byte, error) // return the next line/message
}
