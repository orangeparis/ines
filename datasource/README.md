
# Data sources 

    a set of data source from we can get next message/line with Next()

## interface

    type DataSource interface {
    	// return name of the source
    	Name() string
    	// return the next line/message
    	Next() ([]byte, error)
    }



## datasources

* file :  source is a file
* pipe  : source is linux pipe
* string ?
* tcpsocket ?
* nats ?


