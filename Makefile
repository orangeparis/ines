#include .env

PROJECTNAME=$(shell basename "$(PWD)")

GOBASE=$(shell pwd)
#GOPATH=$(GOBASE)


# PID file will store the server process id when it's running on development mode
NATS_PID=/tmp/.$(PROJECTNAME)-nats-server.pid

# Make is verbose in Linux. Make it silent.
#MAKEFLAGS += --silent

hello :
	echo "Hello ${PROJECTNAME} at ${GOBASE}, GOPATH=${GOPATH}";


start-nats:
	@echo "start nats-server"
	nats-server 2>&1 & echo $$! > $(NATS_PID)
	@cat $(NATS_PID) | sed "/^/s/^/  \>  PID: /"

stop-nats:
	@-touch $(NATS_PID)
	@-kill `cat $(NATS_PID)` 2> /dev/null || true
	@-rm $(NATS_PID)


tests:
	go test bitbucket.org/orangeparis/ines/evs
	go test bitbucket.org/orangeparis/ines/tagfilter
	go test bitbucket.org/orangeparis/ines/datasource
	go test bitbucket.org/orangeparis/ines/nping
	go test bitbucket.org/orangeparis/ines/publisher
	go test bitbucket.org/orangeparis/ines/systemd
	go test bitbucket.org/orangeparis/ines/heartbeat

compile:
	echo "Compiling for every OS and Platform"
	GOOS=linux GOARCH=amd64 go build -o build/packages/linux_amd64/listener ./cmd/listener/listener.go
	GOOS=darwin GOARCH=amd64 go build -o build/packages/darwin_amd64/listener ./cmd/listener/listener.go
