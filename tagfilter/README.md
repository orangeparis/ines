# tagfilter


a filter factory to be able to check a value of a key (tag) in a map[string]string

## interface 

    type TagFilter interface {
    	Filter(map[string]string) (bool, error) // return true if element found , false or err if not
    }

    
## usage 

    var sampleMap = map[string]string{
    	"TAG1":"lorem",
    	"TAG2":"ipsem",
    }
    
    // create a filter
    f1 := tagfilter.NewTagListFilter("TAG1","other","lorem")
    
    // use it 
    ok := f1.Filter(sampleMap)

    
## tagfilters

* taglistfilter return a filter checking value out of a list
* tagregxfilter return a filter checking value out of a regexp
