package tagfilter_test

import (
	"bitbucket.org/orangeparis/ines/tagfilter"
	"testing"
)

func TestTagRegxFilter(t *testing.T) {

	f1, err := tagfilter.NewTagRegxFilter("TAG1", "l.*rem")
	if err != nil {
		t.Fail()
		return
	}
	ok := f1.Filter(sampleMap)
	if ok != true {
		t.Fail()
		return
	}

	f2, err := tagfilter.NewTagRegxFilter("TAG1", "nomatch")
	if err != nil {
		t.Fail()
		return
	}
	ok = f2.Filter(sampleMap)
	if ok != false {
		t.Fail()
		return
	}

	f3, err := tagfilter.NewTagRegxFilter("TAGX", "whatever")
	if err != nil {
		t.Fail()
		return
	}
	ok = f3.Filter(sampleMap)
	if ok != false {
		t.Fail()
		return
	}

}
