package tagfilter

import (
	"log"
	"regexp"
)

type TagRegxFilter struct {
	Tag    string // name of the tag to filtr on ( eg  LogType , InformationalSystem , ErrCode
	Text   string // text of regexp
	regexp *regexp.Regexp
}

// implements tagfilter interface

func (f TagRegxFilter) Filter(data map[string]string) bool {

	var content string
	var ok bool = true

	if content, ok = data[f.Tag]; ok == false {
		// tag not found => return false
		return false
	}
	if f.regexp != nil {
		ok = f.regexp.MatchString(content)
		if ok {
			return true
		}
	} else {
		log.Printf("Invalid Regexp: " + f.Text)
	}

	// dont match
	return false
}

func NewTagRegxFilter(tag string, text string) (f TagRegxFilter, err error) {

	r, err := regexp.Compile(text)
	if err != nil {
		return f, err
	}
	x := TagRegxFilter{Tag: tag, Text: text, regexp: r}
	return x, err
}
