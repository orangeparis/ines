package tagfilter_test

import (
	"bitbucket.org/orangeparis/ines/tagfilter"
	"testing"
)

func TestTagListFilter(t *testing.T) {

	f1 := tagfilter.NewTagListFilter("TAG1", "other", "lorem")

	ok := f1.Filter(sampleMap)
	if ok != true {
		t.Fail()
		return
	}

	// no match
	f2 := tagfilter.NewTagListFilter("TAG1", "other", "another")
	ok = f2.Filter(sampleMap)
	if ok != false {
		t.Fail()
		return
	}

	// inexistant tag
	f3 := tagfilter.NewTagListFilter("TAGX", "other", "another")
	ok = f3.Filter(sampleMap)
	if ok == true {
		t.Fail()
		return
	}

}
