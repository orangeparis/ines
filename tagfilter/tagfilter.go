package tagfilter

/*
	define a tagfilter interface

*/

type TagFilter interface {
	Filter(map[string]string) bool // return true if element found , false or err if not
}
