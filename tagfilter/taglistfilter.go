package tagfilter

type TagListFilter struct {
	Tag    string   // name of the tag to filtr on ( eg  LogType , InformationalSystem , ErrCode
	Values []string // value of LogType to keep , can be one of  FUNCT_TICKET,FUNCT_TICKET_MQ,FUNCT_TICKET_MQ
}

// implements tagfilter interface

func (f TagListFilter) Filter(data map[string]string) bool {

	var content string
	var ok bool = true

	if content, ok = data[f.Tag]; ok == false {
		// tag not found => return false
		return false
	}
	for _, ref := range f.Values {
		if content == ref {
			// keep it
			return true
		}
	}
	// dont match
	return false
}

func NewTagListFilter(tag string, values ...string) TagListFilter {
	x := TagListFilter{Tag: tag, Values: values}
	return x
}
