# ines 

## common libs for ines project


## publisher

    a set of publisher ( nats , file , screen ,nil )
    
## taglist

a filter factory to test key/value in a map[string]string

## datasource

an abstract data source  source.Next([]byte,error)


## nping

     a nats helper to add and request a ping service
     
## heartbeat

    a nats helper to handle heartbeat ( emitter and receiver )
    

## evs 

    an ultra light event-store based on badger and ulid
    