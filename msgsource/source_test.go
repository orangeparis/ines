package msgsource

import (
	"log"
	"os"
	"testing"
	"time"
)

func TestDummyChannelSource(t *testing.T) {

	out := os.Stdout

	// create a Dummy source
	ds := NewDummyChannelSource("dummy", 1024)

	go func() {

		ds.Add("this.is.it", []byte(`{"key":"value"}`))
		m1 := Message{"this.is.one", []byte(`{"key":"value_one"}`)}
		m2 := Message{"this.is.two", []byte(`{"key":"value_two"}`)}

		ds.AddMessage(m1, m2)
		time.Sleep(5 * time.Second)
		log.Printf("Close DatachannelSource\n")
		ds.Close()

	}()

	n := 0
	for {
		m, err := ds.Next()
		if err != nil {
			log.Println(err.Error())
			if n != 3 {
				t.Fail()
				return
			}
			break
		}
		m.Out(out)
		n += 1
	}

}

func TestRangeDummyChannelSource(t *testing.T) {

	// create a Dummy source
	ds := NewDummyChannelSource("dummy", 10)

	go func() {
		ds.Add("this.is.it", []byte(`{"key":"value"}`))

		m1 := Message{"this.is.one", []byte(`{"key":"value_one"}`)}
		m2 := Message{"this.is.two", []byte(`{"key":"value_two"}`)}

		ds.AddMessage(m1, m2)
		time.Sleep(5 * time.Second)
		ds.Close()

	}()

	time.Sleep(6 * time.Second)

	for m := range ds.Channel() {
		m.Display()
	}

	//time.Sleep(10)

}
