package msgsource

import (
	"encoding/json"
	"fmt"
	"io"
)

/*

	define the msg Source interface and Message structure

*/

type Message struct {
	Subject string
	Data    []byte
}

func (m *Message) Unmarshal() (j map[string]interface{}, err error) {
	j = make(map[string]interface{})
	err = json.Unmarshal(m.Data, &j)
	return

}

func (m *Message) Display() {
	fmt.Printf("Message : [%s]\n%s\n", m.Subject, m.Data)
}

func (m *Message) Out(w io.Writer) (err error) {
	msg := fmt.Sprintf("Message : [%s]\n%s\n", m.Subject, m.Data)
	count, err := w.Write([]byte(msg))
	_ = count
	return err
}
