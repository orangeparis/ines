package msgsource

/*

	define the msg Source interface and Message structure

*/

type ChannelSource interface {
	Name() string            // source name
	Next() (*Message, error) // return the next message in channel  ( blocking )
	Channel() chan *Message  // return the Message channel
	Close()
}
