package msgsource

import "errors"

//import "errors"

type DummyChannelSource struct {
	name     string
	Messages chan Message
	count    int
}

func NewDummyChannelSource(name string, channelSize int) (source *DummyChannelSource) {

	ch := make(chan Message, channelSize)
	//ch := make(chan Message)
	source = &DummyChannelSource{name: name, Messages: ch}
	return
}

// implements Source interface

// Name() string          source name
func (s *DummyChannelSource) Name() string {
	return s.name
}
func (s *DummyChannelSource) Channel() chan Message {
	return s.Messages
}

func (s *DummyChannelSource) Close() {
	close(s.Messages)
}

// Next() (*Message , error) return the next line/message ( blocking )
func (s *DummyChannelSource) Next() (m *Message, err error) {

	// blocking next
	select {
	case x := <-s.Messages:
		if x.Subject == "" {
			// read from a closed channel return empty Message
			return &x, errors.New("EOF")
		}
		return &x, nil
	}
}

// convenient funtions for dummy source
func (s *DummyChannelSource) Add(subject string, data []byte) {
	msg := Message{subject, data}
	s.Messages <- msg
	return
}

func (s *DummyChannelSource) AddMessage(m ...Message) {
	for _, n := range m {
		s.Messages <- n
	}
	return
}
