
# msg sources

    a set of data source from we can get next message/line with Next()


## Message

type Message struct {
    Subject string
    Data []byte
}


## interface

    type MsgSource interface {
    	// return name of the source
    	Name() string
    	// return the next line/message
    	Next() (msg *Message, error)
    }



## msgsources

custom


