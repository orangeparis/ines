package process

import (
	"log"
	"testing"
	"time"
)

func watch(process []*Proc) {

	log.Printf("start watcher")
	for {

		for _, p := range process {

			stat := p.IsAlive()
			if stat != true {
				log.Printf("Restarting: %s\n", p.Name)
				err := p.Restart()
				if err != nil {
					log.Printf("Cannot Restart %s\n", p.Name)
				}
			}

		}
		time.Sleep(10 * time.Second)
	}

}

func TestProcessSimple(t *testing.T) {

	args := []string{"-DV"}
	p := &Proc{
		Name: "nats-server",
		Cmd:  "/usr/local/bin/nats-server",
		Args: []string(args),
		Path: "/tmp",
	}

	err := p.Start()
	if err != nil {
		t.Fatalf("failed to start process : %s", err.Error())
	}
	defer p.GracefullyStop()

	p2 := &Proc{
		Name: "redis-server",
		Cmd:  "/usr/local/bin/redis-server",
		Path: "/tmp",
	}
	err = p2.Start()
	if err != nil {
		t.Fatalf("failed to start process : %s", err.Error())
	}
	defer p2.GracefullyStop()

	time.Sleep(10 * time.Second)

	procs := []*Proc{p, p2}
	watch(procs)

	time.Sleep(10 * time.Second)
	//err = p.GracefullyStop()
	//log.Printf("shutdown status: %s\n",err.Error())

	//err = p2.GracefullyStop()
	//log.Printf("shutdown status: %s\n",err.Error())

	print("Done")

}
